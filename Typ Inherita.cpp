#include <iostream>
using namespace std;
class Parent {
  private:
    int pvt = 1;
  protected:
    int prot = 2;
  public:
    int pub = 3;
    int getpvt() {
      return pvt;
    }};
class Child : public Parent{
  public: 
      int getprot() {
      return prot;
    }};
int main() {
  Child object1;
  cout << "Private = " << object1.getpvt() << endl;
  cout << "Protected = " << object1.getprot() << endl;
  cout << "Public = " << object1.pub << endl;
  return 0;
}

